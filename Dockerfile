
FROM httpd:2.4

WORKDIR /usr/local/apache2/htdocs/

COPY *.js ./ 
COPY *.html ./
COPY *.css ./
COPY instance/ instance
COPY my-httpd.conf /usr/local/apache2/conf/httpd.conf
