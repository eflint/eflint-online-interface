var command = function(instance, data, cont) {
  $.post("../server/command", JSON.stringify({
    "uuid" : instance,
    "request-type" : "command",
    "data" : data
  }),
  function(res) {
    var str;
    if (res.status === "SUCCESS") { 
      cont(res.data.response);
      str = JSON.stringify(res.data.response, undefined, 4);
    }
    else {
      str = JSON.stringify(res, undefined, 4);
    }
    $("#command").html("<h2>command output (hidden)</h2><pre class='toggleitem'>"+str+"</pre>");
    $("#command").find("pre").hide();
    $("#command").find("h2").click(function() {
       $(this).parent().find("pre").toggle();
    });
  });
}
var revert = function(instance, state_ref, cont) {
  command(instance, {
      "command" : "revert"
    , "value"   : state_ref 
  }, cont); 
}

var phrases = function(instance, todo, cont) {
  if (todo.length <= 0) {
    cont();
  } else {
    var text = todo.pop();
    phrase(instance, text, function() {
      phrases(instance, todo, cont);
    });
  }
}


var phrase = function(instance, text, cont, onFail) {
  command(instance, {
    "command" : "phrase",
    "text"    : text
  }, function(response) {
      if (response.response && response.response !== "success") {
        var msg = ""; 
        if (response.error) msg = response.error;
        if (response.value) msg = response.value.textual;
        if (response.message) msg = response.message;
        alert(response.response + "\n" + msg);
        onFail(response);
      }
      else {
        if (response.errors && !response.errors.isEmpty) {
          response.errors.forEach(function(error) {
            var msg = ""
            if (error.error) msg = error.error;
            if (error.value) msg = error.value.textual;
            alert(error["error-type"] + "\n" + msg); 
          });
        } 
        cont(response);
      }
  });
}

var current_state = function(instance, cont) {
  command(instance, {
    "command" : "facts"
  }, function(res) {
    if (res.values != null) {
      cont(res.values);
    }
  });
}

var current_history = function(instance, cont) {
  command(instance, {
    "command" : "history"
  }, function(res) {
    if (res.edges != null) {
      cont(res.edges);
    }
  });
}

var start = function(instance, status_res) {
  var editor = $("div#phrase-editor");
  editor.find("button").click(function() {
      phrase(instance, editor.find("textarea").val(), reload);
  });

  current_history(instance, function(edges) {
    display_edges(instance, $("#output"), edges);
  });

  status_res["all-enabled-transitions"].forEach(function(trans) {
    var list = $("div#enabled-transitions").find("ul");
    var elem = $("<li><a class='enabled-transition' href=''>" + trans.textual + "</a></li>");
    elem.find(".enabled-transition-").click(function() {
      phrase(instance, trans.textual, reload); 
    });
    list.append(elem);
  });
  status_res["all-disabled-transitions"].forEach(function(trans) {
    var list = $("div#disabled-transitions").find("ul");
    var elem = $("<li><a class='disabled-transition' href=''>" + trans.textual + "</a></li>");
    elem.find(".disabled-transition").click(function() {
      phrase(instance, "Force " + trans.textual, reload); 
    });
    list.append(elem);
  });
  // show all active duties
  status_res["all-duties"].forEach(function(duty) {
    var list = $("div#active-duties").find("ul");
    var elem = $("<li class='active-duty'>" + duty.textual + "</li>");
    list.append(elem);
  });
  // and highlight the ones that are violated
  status_res.violations.forEach(function(violation) {
    if (violation.violation !== "duty") return;
    $("div#active-duties").find('li:contains(\'' + violation.value.textual + "\')").addClass("duty-violated");
  }); 
}

var display_edges = function(instance, steps, edges){
  // the sequence of phrases that need to be redone after an "update"
  var future_phrases = [];
  
  edges.reverse().forEach(function(edge, index) {
    if (edge.target_id < 0) return;
    var step = $("<div class='step'></div>");
    step.addClass("step" + edge.target_id);
    step.attr("sid", edge.target_id);
    steps.append(step); 
    var action = $("<div class='action'>")
    step.append(action)
    if (edge.target_id == 0) {
      action.append("<div class='instance'>State 0: initial state</div>");
    } else { // target_id > 0
      action.append("<div class='instance'>State " + edge.target_id + "</div>");
    }
    if (edge["output"]) {
      edge["output"].reverse().forEach(function(out) {
        if (out === "success") 
          action.append("<div class='query successful-query'>Query yields true</div>");
        if (out === "failure") 
          action.append("<div class='query unsuccessful-query'>Query yields false</div>");
        if (out["error-type"]) 
          action.append("<div class='error'>" + out["error-type"] + "</div>");
      });
    }
    edge["terminated_facts"].forEach(function(t) {
      action.append($("<div class='tagged-elem terminated'>-" + t.textual + "</div>"));
    });
    edge["created_facts"].forEach(function(c) {
      action.append($("<div class='tagged-elem created'>+" + c.textual + "</div>"));
    });
    edge["violations"].forEach(function(v) {
      var div = $("<div>").addClass("tagged-elem").addClass("violation");
      var li = $("<li>").append($("<span>").html("@ state" + edge.target_id + ", " + v.violation +" violation: "));
      if (v.violation === "invariant") {
        div.addClass('violated-invariant');
        div.append("<span>Violated invariant: " + v.invariant + "</span>");
        action.append(div);
        li.append(div.html());
      }
      else { 
        var type = v.violation === "action" ? "action-violated" : "duty-violated";
        div.addClass(type);
        div.html("Violated: " + v.value.textual);
        action.append(div);
        li.append($("<span>").html(v.value.textual));
      }
      $("#violations > ul").append(li);
    }); 
    var hidden = $("<div class='hidden-contents'>").hide();
    action.append(hidden);
    var code = $(make_phrase(edge.phrase)).addClass("hidden-code");
    hidden.append(code);
    hidden.append(make_revert_button(instance,edge.source_id));
    hidden.append(make_execute_button(instance,edge.source_id,code));
    hidden.append(make_retrace_button(instance,edge.source_id,code,future_phrases.slice()));
    hidden.append(make_remove_button(instance,edge.source_id,future_phrases.slice()));
    state = $("<div class='state'></div>");
    hidden.append(state);
    fill_configuration(instance, state, (edge.target_id == 0) ? [] : edge.target_contents);
    future_phrases.push(edge.phrase);
  });
  // special treatment to current configuration (last in list)
  // steps.children("div.step").first().find(".hidden-contents").toggle();
  // steps.children("div.step").first().find("button.revert").remove();
  // steps.children("div.step").first().find(".violation").fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
  //make action's clickable to open state
  steps.find("div.action > .instance").click(function(item) {
    $(this).parent().closest(".step").find(".hidden-contents").toggle(100);
  });
  // ready loading content via AJAX
  $("div.transparent").removeClass('transparent');
}

var fill_configuration = function(instance, state, contents) {
  if (contents.length <= 0) {
    state.append("empty..");
  }
  else {
    contents.forEach(function(item) {
      state.append("<div class='tagged-elem'>" + item.textual + "</div>"); 
    }); 
  }
}

var make_phrase = function(code) {
  return "<textarea class='source-code'>" + code + "</textarea>";
}

var make_revert_button = function(instance,sid) {
  var button = $("<button class='revert'>Undo</button>");
  button.click(function() {
    if (confirm("Are you sure you wish to undo these and future effects?")) {
      revert(instance, sid, reload);
    }
  });
  return button;
}
var make_execute_button = function(instance,sid,textarea) {
  var button = $("<button class='revert'>Undo & Send phrase</button>");
  button.click(function() {
    if (confirm("Are you sure you wish to undo these and future effects, and execute the written phrase?")) {
      revert(instance, sid, function() {
          phrase(instance, textarea.val(), reload); 
        });
    }
  });
  return button;
}
var make_retrace_button = function(instance,sid,textarea,future_phrases) {
  var button = $("<button class='revert'>Update trace</button>");
  button.click(function() {
    if (confirm("Are you sure you wish to change the executed phrase?")) {
      revert(instance, sid, function() {
          phrase(instance, textarea.val(), function() { // then executed updated phrase
            phrases(instance, future_phrases, reload);  // then redo following phrases
          });
        });
    }
  });
  return button;
}

var make_remove_button = function(instance,sid,future_phrases) {
  var button = $("<button class='revert'>Remove</button>");
  button.click(function() {
    if (confirm("Are you sure you wish to remove this phrase from the trace?")) {
      revert(instance, sid, function() {
          phrases(instance, future_phrases, reload);  // then redo following phrases
        });
    }
  });
  return button;
}

$(document).ready(function() {
  var url_string = window.location.href;
  var url = new URL(url_string);
  var uuid = url.searchParams.get("uuid");
  if (uuid != null) {
    command(uuid, {
      "command" : "status" 
    }, function(response) {
      start(uuid, response);
    });
  }
});


