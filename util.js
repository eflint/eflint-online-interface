var make_instance = function (obj) {
  return {
    "uuid"      : obj.uuid
  , "filename"  : obj['source-file-name'].split("/").pop()
  , "created"   : obj.timestamp
  }
}

var reload = function() { location.reload(); }
var load_instance = function(instance) { location.assign("instance/index.html?uuid=" + instance.uuid);}
var ready = function() {   // ready loading content via AJAX
  $("div.transparent").removeClass('transparent');
}
var loading = function() {   // ready loading content via AJAX
  $("div.transparent").addClass('transparent');
}

