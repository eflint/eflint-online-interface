var start = function () {
  $('#stop-button').click(function() {
    console.log("stopping");
    stop();
  });
  $(window).on("beforeunload", stop);

  var uuid;

  $.post("server/create", JSON.stringify({
    "template-name" : "src/main/resources/templates/empty.eflint"
  }), function(res) {
    if (res.status === "SUCCESS") {
      uuid = res.message;
      console.log(uuid);
      phrase(uuid, "Fact person Identified by Alice, Bob. +person.", generic_handler);
      current_state(uuid, add_facts_to_current_state);
    } 
  }, 'json');

  console.log("done");
}

var generic_handler = function(res) {
  console.log("handler called");
  if (res.response === "success") {
    console.log(res);
  }
}

var stop = function() {
 if (confirm("Are you sure you wish to exit?")) {
  kill_all();
  return true; 
 }
 return false;
}

var command = function(uuid, data, cont) {
  $.post("server/command", JSON.stringify({
    "uuid" : uuid,
    "request-type" : "command",
    "data" : data
  }),
  function(res) {
    if (res.status === "SUCCESS") 
      cont(res.data.response);
  });
}

var phrase = function(uuid, phrase, cont) {
  command(uuid, {
    "command" : "phrase",
    "text"    : phrase
  }, cont);
}

var kill_all = function() {
  $.post("server/kill_all", {}, function(data) {
    return data.status === "SUCCESS";
  }).fail(function() {
    console.log("failed shutdown");
    return false;
  });
}

var add_facts_to_current_state = function(facts) {
  facts.forEach(function(fact) {
//    elem = $("li").addClass("fact-elem").add("test");
    elem = "<li class='fact-elem'>" + fact.textual + "</li>";
    console.log(elem);
    $("#current-state > ul.fact-list").add(elem);
  });
}

$(document).ready(start);

