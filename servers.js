var make_instance_row = function(instance) {
  var row = $("<tr></tr>").attr('id', instance.uuid);
  var cell1 = $("<td></td>").text(instance.uuid);
  var cell2 = $("<td></td>").text(instance.filename);
  var cell3 = $("<td></td>").text(instance.created);
  var explore_button = $("<button>Explore</button>");
  explore_button.click(function() {
    load_instance(instance); 
  });
  var kill_button = $("<button>Kill</button>");
  kill_button.click(function() {
    kill(instance, reload);
  });
  var cell4 = $("<td style='align:right'></td>").append(explore_button,kill_button);
  row.append(cell1,cell2,cell3,cell4);
  return row;
}

var get_all = function(cont) {
  $.get("server/get_all", {}, function(res) {
    if (res.status === "SUCCESS") {
      var instances = []
      res.data.list.forEach(function(obj) {
        instances.push(make_instance(obj))
      }); 
      cont(instances);
    }
  });
}

var kill = function(instance, cont) {
  $.post("server/kill", JSON.stringify({
    "uuid" : instance.uuid
  }), function(res) {
    if (res.status === "SUCCESS")
      cont()
  }, 'json');
}

var create = function(filename, cont) {
  $.post("server/create", JSON.stringify({
    "template-name" : filename,
    "flint-search-paths" : [] 
  }), function(res) {
    if (res.status === "SUCCESS") {
      cont(res.data)
    } 
  }, 'json');
};

$(document).ready(function() {
  get_all(function(instances) {
    instances.forEach(function(instance) {
      $("table#instances").append(make_instance_row(instance));
    });
    if (instances.length > 0) {
      $("table#instances").show();
    }
  });

  $("#fileToUpload").change(function() {
    var file_data = $('#fileToUpload').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('fileToUpload', file_data);
    $.ajax({    url: 'server/upload', 
                dataType: 'json',  
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(res){
                  console.log(res);
                  if (res.status === "SUCCESS") {
                    reload();//load_instance(make_instance(res.message));
                  }
                }
     });
  });

  // button to create fresh instance
  $("button#new-instance").click(function() {
    create("src/main/resources/templates/empty.eflint", function(obj) {
      load_instance(make_instance(obj));
    });
  }); 

  // example links
  $("a.program").click(function() {
    create($(this).attr('rel'), reload);
  });
});
